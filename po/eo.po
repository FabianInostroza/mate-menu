# Esperanto translation for MATE Menu
# Copyright (c) 2010 Rosetta Contributors and Canonical Ltd 2010
# This file is distributed under the same license as the package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2010.
#
msgid ""
msgstr ""
"Project-Id-Version: mate-menu\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-03-11 22:31+0000\n"
"PO-Revision-Date: 2014-05-07 14:28+0000\n"
"Last-Translator: Jon Stevenson <jon.stevenson@gmail.com>\n"
"Language-Team: Esperanto <eo@li.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2015-02-25 10:21+0000\n"
"X-Generator: Launchpad (build 17355)\n"

#: ../lib/mate-menu.py:70
msgid "Menu"
msgstr "Menuo"

#. Fake class for MyPlugin
#: ../lib/mate-menu.py:254
msgid "Couldn't load plugin:"
msgstr "Ne eblis ŝargi kromprogramon:"

#: ../lib/mate-menu.py:326
msgid "Couldn't initialize plugin"
msgstr "Ne eblis pravalorizi kromprogramon"

#: ../lib/mate-menu.py:715
msgid "Advanced MATE Menu"
msgstr ""

#: ../lib/mate-menu.py:798
msgid "Preferences"
msgstr "Agordoj"

#: ../lib/mate-menu.py:801
msgid "Edit menu"
msgstr "Redakti menuon"

#: ../lib/mate-menu.py:804
msgid "Reload plugins"
msgstr "Reŝargi kromprogramojn"

#: ../lib/mate-menu.py:807
msgid "About"
msgstr "Pri"

#. i18n
#: ../lib/mate-menu-config.py:50
msgid "Menu preferences"
msgstr "Menu-agordoj"

#: ../lib/mate-menu-config.py:53
msgid "Always start with favorites pane"
msgstr "Ĉiam starti kun aplikaĵofenestro pri preferitaĵoj"

#: ../lib/mate-menu-config.py:54
msgid "Show button icon"
msgstr "Montri butonsimbolon"

#: ../lib/mate-menu-config.py:55
msgid "Use custom colors"
msgstr "Uzi proprajn kolorojn"

#: ../lib/mate-menu-config.py:56
msgid "Show recent documents plugin"
msgstr "Monti ĵusdokumentan kromprogramon"

#: ../lib/mate-menu-config.py:57
msgid "Show applications plugin"
msgstr "Montri aplikaĵan kromprogramon"

#: ../lib/mate-menu-config.py:58
msgid "Show system plugin"
msgstr "Montri sisteman kromprogramon"

#: ../lib/mate-menu-config.py:59
msgid "Show places plugin"
msgstr "Montri lokan kromprogramon"

#: ../lib/mate-menu-config.py:61
msgid "Show application comments"
msgstr "Montri aplikaĵajn komentojn"

#: ../lib/mate-menu-config.py:62
msgid "Show category icons"
msgstr "Montri katagoriajn piktogramojn"

#: ../lib/mate-menu-config.py:63
msgid "Hover"
msgstr "Musumi"

#: ../lib/mate-menu-config.py:64
msgid "Remember the last category or search"
msgstr "Memori la lastan kategorion aŭ serĉon"

#: ../lib/mate-menu-config.py:65
msgid "Swap name and generic name"
msgstr "Permuta nomo kaj komuna nomo"

#: ../lib/mate-menu-config.py:67
msgid "Border width:"
msgstr "Bordera larĝo:"

#: ../lib/mate-menu-config.py:68
msgid "pixels"
msgstr "rastrumeroj"

#: ../lib/mate-menu-config.py:70
msgid "Opacity:"
msgstr "Travideblo:"

#: ../lib/mate-menu-config.py:73
msgid "Button text:"
msgstr "Butonteksto:"

#: ../lib/mate-menu-config.py:74
msgid "Options"
msgstr "Opcioj"

#: ../lib/mate-menu-config.py:75 ../mate_menu/plugins/applications.py:252
msgid "Applications"
msgstr "Aplikaĵoj"

#: ../lib/mate-menu-config.py:77
msgid "Theme"
msgstr "Etoso"

#: ../lib/mate-menu-config.py:78 ../mate_menu/plugins/applications.py:249
#: ../mate_menu/plugins/applications.py:250
msgid "Favorites"
msgstr "Preferaĵoj"

#: ../lib/mate-menu-config.py:79
msgid "Main button"
msgstr "Ĉefa butono"

#: ../lib/mate-menu-config.py:80
msgid "Plugins"
msgstr ""

#: ../lib/mate-menu-config.py:82
msgid "Background:"
msgstr "Fono:"

#: ../lib/mate-menu-config.py:83
msgid "Headings:"
msgstr "Ĉapoj:"

#: ../lib/mate-menu-config.py:84
msgid "Borders:"
msgstr "Borderoj:"

#: ../lib/mate-menu-config.py:85
msgid "Theme:"
msgstr "Temo:"

#. self.builder.get_object("applicationsLabel").set_text(_("Applications"))
#. self.builder.get_object("favoritesLabel").set_text(_("Favorites"))
#: ../lib/mate-menu-config.py:89
msgid "Number of columns:"
msgstr "Nombro da kolumnoj:"

#: ../lib/mate-menu-config.py:90 ../lib/mate-menu-config.py:91
#: ../lib/mate-menu-config.py:92 ../lib/mate-menu-config.py:93
msgid "Icon size:"
msgstr "Piktograma grando:"

#: ../lib/mate-menu-config.py:94
msgid "Hover delay (ms):"
msgstr "Ŝveba atendo (ms):"

#: ../lib/mate-menu-config.py:95
msgid "Button icon:"
msgstr "Butona piktogramo:"

#: ../lib/mate-menu-config.py:96
msgid "Search command:"
msgstr "Serĉkomando:"

#. Set 'heading' property for plugin
#: ../lib/mate-menu-config.py:98 ../mate_menu/plugins/places.py:58
msgid "Places"
msgstr "Lokoj"

#: ../lib/mate-menu-config.py:99 ../lib/mate-menu-config.py:111
msgid "Allow Scrollbar"
msgstr "Permesi rulumkeston"

#: ../lib/mate-menu-config.py:100
msgid "Show GTK+ Bookmarks"
msgstr "Montri GTK+-paĝosignojn"

#: ../lib/mate-menu-config.py:101 ../lib/mate-menu-config.py:112
msgid "Height:"
msgstr "Alto:"

#: ../lib/mate-menu-config.py:102
msgid "Toggle Default Places:"
msgstr "Baskuligi apriorajn lokojn:"

#: ../lib/mate-menu-config.py:103 ../mate_menu/plugins/places.py:153
msgid "Computer"
msgstr "Komputilo"

#: ../lib/mate-menu-config.py:104 ../mate_menu/plugins/places.py:164
msgid "Home Folder"
msgstr "Hejma dosierujo"

#: ../lib/mate-menu-config.py:105 ../mate_menu/plugins/places.py:177
msgid "Network"
msgstr "Reto"

#: ../lib/mate-menu-config.py:106 ../mate_menu/plugins/places.py:198
msgid "Desktop"
msgstr "Labortablo"

#: ../lib/mate-menu-config.py:107 ../mate_menu/plugins/places.py:209
msgid "Trash"
msgstr "Rubujo"

#: ../lib/mate-menu-config.py:108
msgid "Custom Places:"
msgstr "Propraj lokoj:"

#. Set 'heading' property for plugin
#: ../lib/mate-menu-config.py:110 ../mate_menu/plugins/system_management.py:55
msgid "System"
msgstr "Sistemo"

#: ../lib/mate-menu-config.py:113
msgid "Toggle Default Items:"
msgstr "Baskuligi apriorajn erojn"

#: ../lib/mate-menu-config.py:114
#: ../mate_menu/plugins/system_management.py:149
#: ../mate_menu/plugins/system_management.py:152
#: ../mate_menu/plugins/system_management.py:155
msgid "Package Manager"
msgstr "Pakaĵmastrumilo"

#: ../lib/mate-menu-config.py:115
#: ../mate_menu/plugins/system_management.py:162
msgid "Control Center"
msgstr "Stircentro"

#: ../lib/mate-menu-config.py:116
#: ../mate_menu/plugins/system_management.py:169
msgid "Terminal"
msgstr "Terminalo"

#: ../lib/mate-menu-config.py:117
#: ../mate_menu/plugins/system_management.py:179
msgid "Lock Screen"
msgstr "Ŝlosi ekranon"

#: ../lib/mate-menu-config.py:118
msgid "Log Out"
msgstr "Elsaluti"

#: ../lib/mate-menu-config.py:119
#: ../mate_menu/plugins/system_management.py:197
msgid "Quit"
msgstr "Eliri"

#: ../lib/mate-menu-config.py:121
msgid "Edit Place"
msgstr "Redakti ejon"

#: ../lib/mate-menu-config.py:122
msgid "New Place"
msgstr "Nova ejo"

#: ../lib/mate-menu-config.py:123
msgid "Select a folder"
msgstr "Elektu dosierujon"

#: ../lib/mate-menu-config.py:152
msgid "Keyboard shortcut:"
msgstr "Klavara fulmoklavo:"

#: ../lib/mate-menu-config.py:158
msgid "Images"
msgstr "Bildoj"

#: ../lib/mate-menu-config.py:267
msgid "Name"
msgstr "Nomo"

#: ../lib/mate-menu-config.py:268
msgid "Path"
msgstr "Pado"

#: ../lib/mate-menu-config.py:284
msgid "Desktop theme"
msgstr "Labotabla etoso"

#: ../lib/mate-menu-config.py:432 ../lib/mate-menu-config.py:463
msgid "Name:"
msgstr "Nomo:"

#: ../lib/mate-menu-config.py:433 ../lib/mate-menu-config.py:464
msgid "Path:"
msgstr "Pado:"

#. i18n
#: ../mate_menu/plugins/applications.py:247
msgid "Search:"
msgstr "Serĉi:"

#: ../mate_menu/plugins/applications.py:251
msgid "All applications"
msgstr "Ĉiuj aplikaĵoj"

#: ../mate_menu/plugins/applications.py:618
#, python-format
msgid "Search Google for %s"
msgstr "Serĉi %s ĉe Guglo"

#: ../mate_menu/plugins/applications.py:625
#, python-format
msgid "Search Wikipedia for %s"
msgstr "Serĉi %s ĉe Vikipedio"

#: ../mate_menu/plugins/applications.py:641
#, python-format
msgid "Lookup %s in Dictionary"
msgstr "Serĉi %s en vortaro"

#: ../mate_menu/plugins/applications.py:648
#, python-format
msgid "Search Computer for %s"
msgstr "Serĉi %s en komputilo"

#. i18n
#: ../mate_menu/plugins/applications.py:761
#: ../mate_menu/plugins/applications.py:830
msgid "Add to desktop"
msgstr "Aldoni al labortablo"

#: ../mate_menu/plugins/applications.py:762
#: ../mate_menu/plugins/applications.py:831
msgid "Add to panel"
msgstr "Aldoni al panelo"

#: ../mate_menu/plugins/applications.py:764
#: ../mate_menu/plugins/applications.py:812
msgid "Insert space"
msgstr "Enmetu spacon"

#: ../mate_menu/plugins/applications.py:765
#: ../mate_menu/plugins/applications.py:813
msgid "Insert separator"
msgstr "Enmetu disigilon"

#: ../mate_menu/plugins/applications.py:767
#: ../mate_menu/plugins/applications.py:834
msgid "Launch when I log in"
msgstr "Lanĉi kiam mi ensalutas"

#: ../mate_menu/plugins/applications.py:769
#: ../mate_menu/plugins/applications.py:836
msgid "Launch"
msgstr "Lanĉi"

#: ../mate_menu/plugins/applications.py:770
msgid "Remove from favorites"
msgstr "Forigi el la plej ŝatataj"

#: ../mate_menu/plugins/applications.py:772
#: ../mate_menu/plugins/applications.py:839
msgid "Edit properties"
msgstr "Redakti ecojn"

#. i18n
#: ../mate_menu/plugins/applications.py:811
msgid "Remove"
msgstr "Forigi"

#: ../mate_menu/plugins/applications.py:833
msgid "Show in my favorites"
msgstr "Montri en mia preferitaĵoj"

#: ../mate_menu/plugins/applications.py:837
msgid "Delete from menu"
msgstr "Forigi el menuo"

#: ../mate_menu/plugins/applications.py:888
msgid "Search Google"
msgstr "Serĉi ĉe Guglo"

#: ../mate_menu/plugins/applications.py:895
msgid "Search Wikipedia"
msgstr "Serĉi ĉe Vikipedio"

#: ../mate_menu/plugins/applications.py:905
msgid "Lookup Dictionary"
msgstr "Serĉi en vortaro"

#: ../mate_menu/plugins/applications.py:912
msgid "Search Computer"
msgstr "Serĉi en la komputilo"

#: ../mate_menu/plugins/applications.py:1332
#, fuzzy
msgid ""
"Couldn't save favorites. Check if you have write access to ~/.config/mate-"
"menu"
msgstr ""
"Ne eblis konservi preferitaĵojn. Kontrolu, ĉu vi havas skribaatingon al ~/."
"linuxmint/mintMenu"

#: ../mate_menu/plugins/applications.py:1532
msgid "All"
msgstr "Ĉiuj"

#: ../mate_menu/plugins/applications.py:1532
msgid "Show all applications"
msgstr "Montri ĉiujn aplikaĵojn"

#: ../mate_menu/plugins/system_management.py:159
msgid "Install, remove and upgrade software packages"
msgstr "Instali, forigi kaj promocii programarajn pakaĵojn"

#: ../mate_menu/plugins/system_management.py:166
msgid "Configure your system"
msgstr "Agordi vian sistemon"

#: ../mate_menu/plugins/system_management.py:176
msgid "Use the command line"
msgstr "Uzi la komandlinion"

#: ../mate_menu/plugins/system_management.py:187
msgid "Requires password to unlock"
msgstr "Pasvorto bezonas por malŝlosi"

#: ../mate_menu/plugins/system_management.py:190
msgid "Logout"
msgstr "Elsaluti"

#: ../mate_menu/plugins/system_management.py:194
msgid "Log out or switch user"
msgstr "Elsaluti aŭ ŝanĝi uzanton"

#: ../mate_menu/plugins/system_management.py:201
msgid "Shutdown, restart, suspend or hibernate"
msgstr "Elŝalti, restartigi, paŭzigi aŭ pasivumigi"

#: ../mate_menu/plugins/places.py:161
msgid ""
"Browse all local and remote disks and folders accessible from this computer"
msgstr ""
"Foliumi ĉiujn lokan kaj foran diskojn kaj dosierujojn atingeblajn de ĉi tiu "
"komputilo"

#: ../mate_menu/plugins/places.py:172
msgid "Open your personal folder"
msgstr "Malfermi vian personan dosierujon"

#: ../mate_menu/plugins/places.py:185
msgid "Browse bookmarked and local network locations"
msgstr "Foliumi paĝosignatan kaj lokan retlokojn"

#: ../mate_menu/plugins/places.py:206
msgid "Browse items placed on the desktop"
msgstr "Foliumi erojn metitajn en la labortablo"

#: ../mate_menu/plugins/places.py:219
msgid "Browse deleted files"
msgstr "Foliumi forigitajn dosierojn"

#: ../mate_menu/plugins/places.py:272
msgid "Empty trash"
msgstr "Malplenigi rubujon"

#. Set 'heading' property for plugin
#: ../mate_menu/plugins/recent.py:48
msgid "Recent documents"
msgstr "Ĵusaj dokumentoj"

#: ../mate_menu/keybinding.py:199
msgid "Click to set a new accelerator key for opening and closing the menu.  "
msgstr ""
"Alklaku por asigni novan fulmoklavon por malfermi kaj fermi la menuon  "

#: ../mate_menu/keybinding.py:200
msgid "Press Escape or click again to cancel the operation.  "
msgstr "Premu Esc aŭ klaku denove por nuligi  "

#: ../mate_menu/keybinding.py:201
msgid "Press Backspace to clear the existing keybinding."
msgstr "Premu Retropaŝo por liberigi la aktuala klavo-bindado"

#: ../mate_menu/keybinding.py:214
msgid "Pick an accelerator"
msgstr "Elektu fulmoklavon"

#: ../mate_menu/keybinding.py:267
msgid "<not set>"
msgstr "<sen agordo>"

#~ msgid "Search for packages to install"
#~ msgstr "Serĉi instalendajn pakaĵojn"

#~ msgid "Software Manager"
#~ msgstr "Programarmastrumilo"

#~ msgid "Install package '%s'"
#~ msgstr "Instali pakaĵon '%s'"

#~ msgid "Uninstall"
#~ msgstr "Malinstali"

#~ msgid "Find Software"
#~ msgstr "Trovi programaron"

#~ msgid "Find Tutorials"
#~ msgstr "Trovi lernilon"

#~ msgid "Find Hardware"
#~ msgstr "Trovi aparataron"

#~ msgid "Find Ideas"
#~ msgstr "Trovi ideojn"

#~ msgid "Find Users"
#~ msgstr "Trovi uzantojn"

#~ msgid "Browse and install available software"
#~ msgstr "Foliumi kaj instali disponeblajn programarojn"

#~ msgid "Remove %s?"
#~ msgstr "Ĉu forigi %s?"

#~ msgid "Colors"
#~ msgstr "Koloroj"

#~ msgid "File not found"
#~ msgstr "Ne trovis dosieron"

#~ msgid "Based on USP from S.Chanderbally"
#~ msgstr "Baziĝita sur USP el S.Chanderbally"

#~ msgid "Please wait, this can take some time"
#~ msgstr "Bonvole atendu, necesas iom da tempo"

#~ msgid "Advanced Gnome Menu"
#~ msgstr "Porspertulaj GNOME-menuo"

#~ msgid "Application removed successfully"
#~ msgstr "Aplikaĵo sukcese forigita"

#~ msgid "No matching package found"
#~ msgstr "Neniu kongrua pakaĵo trovata"

#~ msgid "Do you want to remove this menu entry?"
#~ msgstr "Ĉu vi volas forigi ĉi tiun menueron?"

#~ msgid "Packages to be removed"
#~ msgstr "Forigotaj pakaĵoj"

#~ msgid "The following packages will be removed:"
#~ msgstr "La jenaj pakaĵoj estos forigataj:"
